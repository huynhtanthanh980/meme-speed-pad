import 'dart:async';
import 'dart:core';

import 'package:meme_speed/Model/CustomButton.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class ButtonData {
  Database buttonData;

  initDatabase() async {
    this.buttonData = await openDatabase(
      join(await getDatabasesPath(), 'button_database.db'),
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE button(id INTEGER PRIMARY KEY, name TEXT, path TEXT, isDisabled BOOLEAN, repeat BOOLEAN, multiPlay BOOLEAN, buttonColor INTEGER, textColor INTEGER, speed float, volume float)",
        );
      },
      version: 1,
    );
  }

  Future<void> addButton(CustomButton button) async {
    await buttonData.insert(
      'button',
      {
        'id': button.id,
        'name': button.name,
        'path': button.path,
        'isDisabled': button.isDisabled,
        'repeat': button.repeat,
        'multiPlay': button.multiPlay,
        'buttonColor': button.buttonColor,
        'textColor': button.textColor,
        'speed': button.speed,
        'volume': button.volume,
      },
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<CustomButton>> getButtonList() async {
    final List<Map<String, dynamic>> maps = await buttonData.query('button');

    return List.generate(
      maps.length,
      (i) => CustomButton(
        id: maps[i]['id'],
        name: maps[i]['name'],
        path: maps[i]['path'],
        isDisabled: (maps[i]['isDisabled'] == 1) ? true : false,
        repeat: (maps[i]['repeat'] == 1) ? true : false,
        multiPlay: (maps[i]['multiPlay'] == 1) ? true : false,
        buttonColor: maps[i]['buttonColor'],
        textColor: maps[i]['textColor'],
        speed: maps[i]['speed'],
        volume: maps[i]['volume'],
      ),
    );
  }

  Future<void> updateButton(CustomButton button) async {
    await buttonData.update(
      'button',
      button.toMap(),
      where: "id = ?",
      whereArgs: [button.id],
    );
  }

  Future<void> removeButton(int id) async {
    await buttonData.delete(
      'button',
      where: "id = ?",
      whereArgs: [id],
    );
  }
}
