import 'package:flutter/material.dart';

class CustomButton {
  int id;
  String name;
  String path;
  bool isDisabled;
  bool repeat;
  bool multiPlay;
  int buttonColor;
  int textColor;
  double speed = 1.0;
  double volume = 1.0;

  CustomButton({
    @required this.id,
    @required this.name,
    @required this.path,
    @required this.isDisabled,
    @required this.repeat,
    @required this.multiPlay,
    @required this.buttonColor,
    @required this.textColor,
    @required this.speed,
    @required this.volume,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'path': path,
      'isDisabled': isDisabled ? 1 : 0,
      'repeat': repeat ? 1 : 0,
      'multiPlay': multiPlay ? 1 : 0,
      'buttonColor': buttonColor,
      'textColor': textColor,
      'speed': speed,
      'volume': volume,
    };
  }

  bool isRegistered() {
    return name.isNotEmpty && path.isNotEmpty;
  }

  void clear() {
    name = "";
    path = "";
    isDisabled = false;
    repeat = false;
    multiPlay = false;
    buttonColor = null;
    textColor = null;
    speed = 1.0;
    volume = 1.0;
  }
}
