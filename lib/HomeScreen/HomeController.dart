import 'dart:async';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:meme_speed/Bottom_Menu/BottomMenuWidget.dart';
import 'package:meme_speed/Data/ButtonData.dart';
import 'package:meme_speed/Model/CustomButton.dart';
import 'package:path/path.dart' as pathLib;
import 'package:rxdart/rxdart.dart';

class HomeController {
  int _padNum = 0;

  ButtonData _data = ButtonData();

  List<CustomButton> _padButtonList = List<CustomButton>(45);
  List<AudioPlayer> _tempPlayer =
      List<AudioPlayer>.generate(45, (index) => AudioPlayer());

  List<List<AudioPlayer>> _playHistory =
      List<List<AudioPlayer>>.generate(45, (index) => List<AudioPlayer>());

  HomeController() {
    _buttonController.stream.listen(_buildNewButton);
    _fileSelectController.stream.listen(_getFileName);
  }

  String getButtonName(int buttonIndex) {
    return _padButtonList[buttonIndex].name;
  }

  String getButtonPath(int buttonIndex) {
    return _padButtonList[buttonIndex].path;
  }

  bool isButtonDisable(int buttonIndex) {
    return _padButtonList[buttonIndex].isDisabled;
  }

  bool isMultiPlay(int buttonIndex) {
    return _padButtonList[buttonIndex].multiPlay;
  }

  bool isRepeat(int buttonIndex) {
    return _padButtonList[buttonIndex].repeat;
  }

  int getButtonColorValue(int buttonIndex) {
    return _padButtonList[buttonIndex].buttonColor;
  }

  int getTextColorValue(int buttonIndex) {
    return _padButtonList[buttonIndex].textColor;
  }

  double getButtonSpeed(int buttonIndex) {
    return _padButtonList[buttonIndex].speed;
  }

  double getButtonVolume(int buttonIndex) {
    return _padButtonList[buttonIndex].volume;
  }

  bool isButtonRegistered(int buttonIndex) {
    return _padButtonList[buttonIndex].isRegistered();
  }

  Future<bool> createPadButton(
      int index,
      String name,
      String path,
      bool isDisabled,
      bool repeat,
      bool multiPlay,
      int buttonColor,
      int textColor,
      double speed,
      double volume) async {
    CustomButton newButton = CustomButton(
      id: index,
      name: name,
      path: path,
      isDisabled: isDisabled,
      repeat: repeat,
      multiPlay: multiPlay,
      buttonColor: buttonColor,
      textColor: textColor,
      speed: speed,
      volume: volume,
    );

    _padButtonList[index] = newButton;

    await _data.addButton(newButton);

    changeButton.add(index);

    return true;
  }

  Future<bool> updatePadButton(
      int index,
      String name,
      String path,
      bool isDisabled,
      bool repeat,
      bool multiPlay,
      int buttonColor,
      int textColor,
      double speed,
      double volume) async {
    CustomButton newButton = CustomButton(
      id: index,
      name: name,
      path: path,
      isDisabled: isDisabled,
      repeat: repeat,
      multiPlay: multiPlay,
      buttonColor: buttonColor,
      textColor: textColor,
      speed: speed,
      volume: volume,
    );

    _padButtonList[index] = newButton;

    await _data.updateButton(newButton);

    changeButton.add(index);

    return true;
  }

  Future<bool> removePadButton(int index) async {
    _padButtonList[index].clear();
    changeButton.add(index);
    return true;
  }

  Future<bool> initHomeController() async {
    await _data.initDatabase();
    List<CustomButton> list = await _data.getButtonList();

    for (CustomButton button in list) {
      if (await File(button.path).exists()) {
        _padButtonList[button.id] = button;
      } else {
        await _data.removeButton(button.id);
      }
    }

    for (var index = 0; index < 45; index++) {
      if (_padButtonList[index] == null) {
        await createPadButton(
            index, "", "", false, false, false, null, null, 1.0, 1.0);
      }
    }

    return true;
  }

  Future<AudioPlayer> createPlayer(String path) async {
    AudioPlayer player = AudioPlayer();
    await player.setFilePath(path);
    return player;
  }

  int getPadIndex(int index) {
    return index + _padNum * 15;
  }

  Future<void> onPadButtonPressed(int buttonIndex) async {
    if (_padButtonList[buttonIndex].isRegistered()) {
      //Multi player or no player
      if ((_padButtonList[buttonIndex].multiPlay ||
              _playHistory[buttonIndex].length == 0) &&
          _padButtonList[buttonIndex].isRegistered()) {
        CustomButton padButton = _padButtonList[buttonIndex];

        AudioPlayer player = new AudioPlayer();
        await player.setFilePath(padButton.path);
        await player.setSpeed(padButton.speed);
        await player.setVolume(padButton.volume);

        //add to list for stop all
        _playHistory[buttonIndex].add(player);

        playMeme(buttonIndex, player);
      }
      //Single player
      else {
        if (_playHistory[buttonIndex].length == 1) {
          await stopMeme(buttonIndex, _playHistory[buttonIndex].first);
        } else {
          stopInList(buttonIndex);
        }
      }
    }
  }

  Future<void> showButtonMenu(BuildContext context, int buttonIndex) {
    return showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (context) {
        return BottomMenu(this, buttonIndex, getTempPlayer(buttonIndex));
      },
    );
  }

  Future<void> playMeme(int buttonIndex, AudioPlayer player) async {
    await player.play();

    //repeat
    if (_padButtonList[buttonIndex].repeat &&
        player.playbackState != AudioPlaybackState.stopped) {
      await player.seek(Duration(milliseconds: 0));
      await playMeme(buttonIndex, player);
    }
    //after player is completed or stopped
    else if (player != null) {
      await stopMeme(buttonIndex, player);
    }
  }

  Future<void> stopMeme(int buttonIndex, AudioPlayer player) async {
    _playHistory[buttonIndex].remove(player);
    await player.stop();
    player.dispose();
  }

  void stopInList(int buttonIndex) {
    for (AudioPlayer player in _playHistory[buttonIndex]) {
      player.stop();
    }
    _playHistory[buttonIndex].clear();
  }

  void stopAll() {
    for (List<AudioPlayer> list in _playHistory) {
      for (AudioPlayer x in list) {
        x.stop();
      }
    }
    for (AudioPlayer x in _tempPlayer) {
      x.stop();
    }
  }

  void changeSinglePlayerSpeed(int buttonIndex, double newSpeed) {
    if (!_padButtonList[buttonIndex].multiPlay &&
        _playHistory[buttonIndex].length == 1) {
      _playHistory[buttonIndex].first.setSpeed(newSpeed);
    }
  }

  void changeSinglePlayerVolume(int buttonIndex, double newVolume) {
    if (!_padButtonList[buttonIndex].multiPlay &&
        _playHistory[buttonIndex].length == 1) {
      _playHistory[buttonIndex].first.setVolume(newVolume);
    }
  }

  AudioPlayer getTempPlayer(int index) {
    if (isButtonRegistered(index) &&
        _tempPlayer[index].playbackState == AudioPlaybackState.none) {
      _tempPlayer[index].setFilePath(_padButtonList[index].path);
    }
    return _tempPlayer[index];
  }

  Sink<int> get changeButton => _buttonController.sink;
  final _buttonController = StreamController<int>();

  Stream<void> get getButtonBuild => _getButtonBuildSubject.stream;
  final _getButtonBuildSubject = BehaviorSubject<void>();

  void _buildNewButton(int index) {
    _getButtonBuildSubject.add(null);
  }

  Sink<String> get selectFile => _fileSelectController.sink;
  final _fileSelectController = StreamController<String>();

  Stream<String> get getFileName => _getFileNameSubject.stream;
  final _getFileNameSubject = BehaviorSubject<String>();

  void _getFileName(String fileName) {
    _getFileNameSubject.add(fileName);
  }

  Future<String> browseFile() async {
    File file = await FilePicker.getFile();

    if (pathLib.extension(file.path) == ".mp3") {
      String fileName = pathLib.basename(file.path);
      selectFile.add(fileName);
      return file.path;
    }

    return "";
  }

  Color getButtonColor(int buttonIndex) {
    int buttonColor = getButtonColorValue(buttonIndex);

    if (buttonColor == null) {
      if (buttonIndex < 15) {
        return Colors.green;
      } else if (buttonIndex >= 15 && buttonIndex < 30) {
        return Colors.blue;
      } else {
        return Colors.red;
      }
    } else {
      return Color(buttonColor);
    }
  }

  Color getTextColor(int buttonIndex) {
    int textColor = getTextColorValue(buttonIndex);

    if (textColor == null) {
      return Colors.black;
    } else {
      return Color(textColor);
    }
  }

  void changePad(int index) {
    if (index > 3 || index < 0) {
      return;
    }

    _padNum = index;

    changeButton.add(0);
  }

  dispose() {
    stopAll();
    _buttonController.close();
    _getButtonBuildSubject.close();
    _fileSelectController.close();
    _getFileNameSubject.close();
  }
}
