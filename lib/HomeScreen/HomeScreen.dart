import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:meme_speed/HomeScreen/HomeController.dart';
import 'package:meme_speed/Model/ScreenSize.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen> {
  HomeController homeController;

  CarouselSlider carouselSlider;

  double speed;
  double volume;

  @override
  void initState() {
    homeController = new HomeController();
    super.initState();
  }

  @override
  dispose() {
    homeController.dispose();
    super.dispose();
  }

  // ignore: missing_return
  Future<int> whenNotZero(Stream<double> source) async {
    await homeController.initHomeController();
    await for (double value in source) {
      if (value > 0) {
        return 1; //if screen size is found, return 1
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (ScreenSize.isInit) {
      return buildBody();
    } else {
      return FutureBuilder(
        future: whenNotZero(Stream<double>.periodic(Duration(milliseconds: 50),
            (x) => MediaQuery.of(context).size.width)),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            ScreenSize().init(context);
            return Scaffold(
              resizeToAvoidBottomPadding: false,
              backgroundColor: Colors.black,
              body: buildBody(),
            );
          }
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        },
      );
    }
  }

  Widget buildBody() {
    carouselSlider = CarouselSlider(
      height: ScreenSize.verticalBlockSize * 85,
      aspectRatio: ScreenSize.verticalBlockSize *
          85 /
          ScreenSize.horizontalBlockSize *
          100,
      viewportFraction: 1.0,
      items: List.generate(3, (index) => buildPad(index)),
      onPageChanged: (index) {
        homeController.changePad(index);
      },
    );
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            height: ScreenSize.verticalBlockSize * 85,
            width: ScreenSize.horizontalBlockSize * 100,
            child: carouselSlider,
          ),
          Container(
            height: ScreenSize.verticalBlockSize * 15,
            width: ScreenSize.horizontalBlockSize * 100,
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: ScreenSize.verticalBlockSize * 12,
                      maxWidth: ScreenSize.horizontalBlockSize * 20,
                    ),
                    child: AspectRatio(
                      aspectRatio: 1.0,
                      child: MaterialButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color: Colors.green,
                        onPressed: () {
                          carouselSlider.animateToPage(0,
                              duration: Duration(
                                milliseconds: 200,
                              ),
                              curve: Curves.easeIn);
                        },
                      ),
                    ),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: ScreenSize.verticalBlockSize * 12,
                      maxWidth: ScreenSize.horizontalBlockSize * 20,
                    ),
                    child: AspectRatio(
                      aspectRatio: 1.0,
                      child: MaterialButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color: Colors.blue,
                        onPressed: () {
                          carouselSlider.animateToPage(1,
                              duration: Duration(
                                milliseconds: 200,
                              ),
                              curve: Curves.easeIn);
                        },
                      ),
                    ),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: ScreenSize.verticalBlockSize * 12,
                      maxWidth: ScreenSize.horizontalBlockSize * 20,
                    ),
                    child: AspectRatio(
                      aspectRatio: 1.0,
                      child: MaterialButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color: Colors.red,
                        onPressed: () {
                          carouselSlider.animateToPage(2,
                              duration: Duration(
                                milliseconds: 200,
                              ),
                              curve: Curves.easeIn);
                        },
                      ),
                    ),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: ScreenSize.verticalBlockSize * 12,
                      maxWidth: ScreenSize.horizontalBlockSize * 20,
                    ),
                    child: AspectRatio(
                      aspectRatio: 1.0,
                      child: MaterialButton(
                        shape: CircleBorder(),
                        color: Colors.white,
                        onPressed: () {
                          homeController.stopAll();
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildPad(int padIndex) {
    return Container(
      height: ScreenSize.verticalBlockSize * 85,
      width: ScreenSize.horizontalBlockSize * 100,
      padding: EdgeInsets.symmetric(
        vertical: ScreenSize.verticalBlockSize * 1,
      ),
      alignment: Alignment(0.0, 0.0),
      child: StreamBuilder(
        stream: homeController.getButtonBuild,
        builder: (context, snapshot) {
          return Wrap(
            spacing: ScreenSize.horizontalBlockSize * 2,
            runSpacing: ScreenSize.verticalBlockSize * 2,
            children: new List.generate(
                15, (index) => buildPadButton(padIndex, index + padIndex * 15)),
          );
        },
      ),
    );
  }

  Widget buildPadButton(int padIndex, int buttonIndex) {
    String name = homeController.getButtonName(buttonIndex);
    bool isDisable = homeController.isButtonDisable(buttonIndex);
    bool isRegistered = homeController.isButtonRegistered(buttonIndex);

    return ConstrainedBox(
      constraints: BoxConstraints(
        maxHeight: ScreenSize.verticalBlockSize * 15,
        maxWidth: ScreenSize.horizontalBlockSize * 30,
      ),
      child: AspectRatio(
        aspectRatio: 1.0,
        child: (isDisable)
            ? GestureDetector(
                onLongPress: () {
                  homeController.showButtonMenu(context, buttonIndex);
                },
              )
            : MaterialButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  side: BorderSide(
                    width: (isRegistered) ? 0.0 : 1.0,
                    color: homeController.getButtonColor(buttonIndex),
                  ),
                ),
          color: (!isRegistered)
                    ? Colors.black
              : homeController.getButtonColor(buttonIndex),
          onPressed: () async {
            await homeController.onPadButtonPressed(buttonIndex);
          },
          onLongPress: () async {
            await homeController.showButtonMenu(context, buttonIndex);
                },
                child: Container(
                  child: Center(
                    child: Text(
                      name ?? "",
                      overflow: TextOverflow.clip,
                      style: TextStyle(
                        color: homeController.getTextColor(buttonIndex),
                      ),
                    ),
                  ),
                ),
        ),
      ),
    );
  }
}
