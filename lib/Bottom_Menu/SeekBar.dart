import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';

class SeekBar extends StatefulWidget {
  final AudioPlayer player;

  SeekBar({
    @required this.player,
  });

  @override
  _SeekBarState createState() => _SeekBarState();
}

class _SeekBarState extends State<SeekBar> {
  double _dragValue;
  bool dragLock = false;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Duration>(
      stream: widget.player.durationStream,
      builder: (context, snapshot) {
        final duration = snapshot.data ?? Duration.zero;
        return StreamBuilder<Duration>(
          stream: widget.player.getPositionStream(),
          builder: (context, snapshot) {
            var position = snapshot.data ?? Duration.zero;
            if (position > duration) {
              position = duration;
            }
            if (!dragLock) {
              _dragValue = position.inMilliseconds.toDouble();
            }
            return Slider(
              min: 0.0,
              max: duration.inMilliseconds.toDouble(),
              value: _dragValue,
              label: _dragValue.round().toString(),
              onChanged: (value) async {
                dragLock = true;
                if (widget.player.playbackState == AudioPlaybackState.playing) {
                  _dragValue = value;
                } else {
                  //set player to pause
                  await widget.player.play();
                  setState(() {
                    _dragValue = value;
                  });
                }
                widget.player.seek(Duration(milliseconds: value.round()));
              },
              onChangeEnd: (value) {
                dragLock = false;
              },
            );
          },
        );
      },
    );
  }
}
