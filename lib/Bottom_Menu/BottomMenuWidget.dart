import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_material_color_picker/flutter_material_color_picker.dart';
import 'package:just_audio/just_audio.dart';
import 'package:meme_speed/Bottom_Menu/BottomMenuController.dart';
import 'package:meme_speed/Bottom_Menu/SeekBar.dart';
import 'package:meme_speed/HomeScreen/HomeController.dart';
import 'package:meme_speed/Model/ScreenSize.dart';

class BottomMenu extends StatefulWidget {
  final HomeController homeController;
  final int buttonIndex;
  final AudioPlayer tempPlayer;

  BottomMenu(
    this.homeController,
    this.buttonIndex,
    this.tempPlayer,
  );

  @override
  State<StatefulWidget> createState() {
    return BottomMenuState();
  }
}

class BottomMenuState extends State<BottomMenu> {
  BottomMenuController menuController;

  @override
  void initState() {
    menuController = BottomMenuController(
      homeController: widget.homeController,
      buttonIndex: widget.buttonIndex,
      tempPlayer: widget.tempPlayer,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    menuController.carouselSlider = CarouselSlider(
      height: ScreenSize.verticalBlockSize * 35,
      viewportFraction: 1.0,
      scrollPhysics: NeverScrollableScrollPhysics(),
      items: <Widget>[
        buildMenu(),
        buildColorSelect(),
      ],
    );

    return SingleChildScrollView(
      child: Container(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Container(
          height: ScreenSize.verticalBlockSize * 35,
          padding: EdgeInsets.symmetric(
            vertical: ScreenSize.verticalBlockSize * 2,
            horizontal: ScreenSize.horizontalBlockSize,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0),
              topRight: Radius.circular(20.0),
            ),
            color: Colors.white,
          ),
          child: menuController.carouselSlider,
        ),
      ),
    );
  }

  Widget buildMenu() {
    return Container(
      height: ScreenSize.verticalBlockSize * 31,
      width: ScreenSize.horizontalBlockSize * 80,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                height: ScreenSize.verticalBlockSize * 4,
                width: ScreenSize.horizontalBlockSize * 80,
                child: Row(
                  children: <Widget>[
                    Container(
                      height: ScreenSize.verticalBlockSize * 4,
                      width: ScreenSize.horizontalBlockSize * 60,
                      padding: EdgeInsets.symmetric(
                        horizontal: ScreenSize.horizontalBlockSize,
                      ),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.grey,
                          width: ScreenSize.horizontalBlockSize * 0.5,
                        ),
                      ),
                      child: Center(
                        child: TextFormField(
                          controller: menuController.textController,
                          decoration: InputDecoration(
                            hintText: "Name",
                            border: InputBorder.none,
                          ),
                          textAlign: TextAlign.center,
                          textAlignVertical: TextAlignVertical.center,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: ScreenSize.horizontalBlockSize * 2,
                    ),
                    Container(
                      height: ScreenSize.verticalBlockSize * 4,
                      width: ScreenSize.horizontalBlockSize * 7,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.black12,
                      ),
                      child: Center(
                        child: MaterialButton(
                          padding: EdgeInsets.zero,
                          shape: CircleBorder(),
                          child: Icon(
                            Icons.clear,
                            size: ScreenSize.horizontalBlockSize * 4,
                          ),
                          onPressed: menuController.clearTextFiled,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: ScreenSize.horizontalBlockSize * 2,
                    ),
                    Container(
                      height: ScreenSize.verticalBlockSize * 4,
                      width: ScreenSize.horizontalBlockSize * 7,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                      child: StreamBuilder(
                        stream: menuController.getTextColorStream,
                        builder: (context, snapshot) {
                          return FlatButton(
                            padding: EdgeInsets.zero,
                            shape: CircleBorder(
                              side: BorderSide(color: Colors.black),
                            ),
                            color: snapshot?.data ??
                                widget.homeController
                                    .getTextColor(widget.buttonIndex),
                            child: Container(),
                            onPressed: () {
                              menuController.selectColor(false);
                            },
                          );
                        },
                      ),
                    ),
                    SizedBox(
                      width: ScreenSize.horizontalBlockSize * 2,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: ScreenSize.verticalBlockSize * 1,
              ),
              Container(
                height: ScreenSize.verticalBlockSize * 4,
                width: ScreenSize.horizontalBlockSize * 80,
                color: Colors.black12,
                child: Row(
                  children: <Widget>[
                    Container(
                      height: ScreenSize.verticalBlockSize * 4,
                      width: ScreenSize.horizontalBlockSize * 60,
                      child: Center(
                        child: StreamBuilder(
                          stream: widget.homeController.getFileName,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              return Text(
                                snapshot.data,
                                style: TextStyle(
                                  fontSize: ScreenSize.verticalBlockSize * 1.5,
                                ),
                              );
                            }
                            return Text(
                              "No file selected",
                              style: TextStyle(
                                fontSize: ScreenSize.verticalBlockSize * 1.5,
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                    Container(
                      height: ScreenSize.verticalBlockSize * 4,
                      width: ScreenSize.horizontalBlockSize * 20,
                      padding: EdgeInsets.symmetric(
                        vertical: ScreenSize.verticalBlockSize * 0.5,
                        horizontal: ScreenSize.horizontalBlockSize * 1,
                      ),
                      child: FlatButton(
                        padding: EdgeInsets.zero,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.black54),
                        ),
                        color: Colors.white10,
                        child: Text(
                          "Browse",
                          style: TextStyle(
                            fontSize: ScreenSize.verticalBlockSize * 1.5,
                          ),
                        ),
                        onPressed: () async =>
                            await menuController.browseFile(),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: ScreenSize.verticalBlockSize * 4,
                width: ScreenSize.horizontalBlockSize * 80,
                color: Colors.black12,
                child: Row(
                  children: <Widget>[
                    Container(
                      height: ScreenSize.verticalBlockSize * 4,
                      width: ScreenSize.horizontalBlockSize * 60,
                      child: Center(
                        child: SeekBar(
                          player: menuController.tempPlayer,
                        ),
                      ),
                    ),
                    Container(
                      height: ScreenSize.verticalBlockSize * 4,
                      width: ScreenSize.horizontalBlockSize * 20,
                      child: Row(
                        children: <Widget>[
                          SizedBox(
                            width: ScreenSize.horizontalBlockSize * 2,
                          ),
                          Container(
                            height: ScreenSize.verticalBlockSize * 4,
                            width: ScreenSize.horizontalBlockSize * 7,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.black12,
                            ),
                            child: Center(
                              child: StreamBuilder(
                                stream: menuController
                                    .tempPlayer.playbackStateStream,
                                builder: (context, snapshot) {
                                  AudioPlaybackState state =
                                      menuController.getPlayerState();
                                  return MaterialButton(
                                    padding: EdgeInsets.zero,
                                    shape: CircleBorder(),
                                    child: Icon(
                                      (state == AudioPlaybackState.playing)
                                          ? Icons.pause
                                          : Icons.play_arrow,
                                      size: ScreenSize.horizontalBlockSize * 4,
                                      color: (widget.tempPlayer.playbackState ==
                                              AudioPlaybackState.none)
                                          ? Colors.black26
                                          : Colors.black,
                                    ),
                                    onPressed:
                                        menuController.onPlayButtonPressed,
                                  );
                                },
                              ),
                            ),
                          ),
                          SizedBox(
                            width: ScreenSize.horizontalBlockSize * 2,
                          ),
                          Container(
                            height: ScreenSize.verticalBlockSize * 4,
                            width: ScreenSize.horizontalBlockSize * 7,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.black12,
                            ),
                            child: Center(
                              child: StreamBuilder(
                                stream: menuController
                                    .tempPlayer.playbackStateStream,
                                builder: (context, snapshot) {
                                  return MaterialButton(
                                    padding: EdgeInsets.zero,
                                    shape: CircleBorder(),
                                    child: Icon(
                                      Icons.stop,
                                      size: ScreenSize.horizontalBlockSize * 4,
                                      color: (widget.tempPlayer.playbackState ==
                                              AudioPlaybackState.none)
                                          ? Colors.black26
                                          : Colors.black,
                                    ),
                                    onPressed: () async {
                                      await menuController.stopMeme();
                                    },
                                  );
                                },
                              ),
                            ),
                          ),
                          SizedBox(
                            width: ScreenSize.horizontalBlockSize * 2,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: ScreenSize.verticalBlockSize * 2,
              ),
              Container(
                height: ScreenSize.verticalBlockSize * 5,
                width: ScreenSize.horizontalBlockSize * 80,
                child: Row(
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Speed",
                          style: TextStyle(
                            fontSize: ScreenSize.verticalBlockSize * 1.5,
                          ),
                        ),
                        SizedBox(
                          height: ScreenSize.verticalBlockSize * 1,
                        ),
                        Text(
                          "Volume",
                          style: TextStyle(
                            fontSize: ScreenSize.verticalBlockSize * 1.5,
                          ),
                        ),
                      ],
                    ),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: ScreenSize.verticalBlockSize * 2,
                            width: ScreenSize.horizontalBlockSize * 70,
                            child: StreamBuilder(
                              stream: menuController.getSpeedTitle,
                              builder: (context, snapshot) {
                                return Slider(
                                  value: menuController.speed,
                                  max: 2.0,
                                  min: 0.5,
                                  divisions: 15,
                                  label: snapshot.data,
                                  onChanged: menuController.onSpeedSliderChange,
                                );
                              },
                            ),
                          ),
                          SizedBox(
                            height: ScreenSize.verticalBlockSize * 1,
                          ),
                          Container(
                            height: ScreenSize.verticalBlockSize * 2,
                            width: ScreenSize.horizontalBlockSize * 70,
                            child: StreamBuilder(
                              stream: menuController.getVolumeTitleStream,
                              builder: (context, snapshot) {
                                return Slider(
                                  value: menuController.volume,
                                  max: 1.0,
                                  min: 0.0,
                                  divisions: 10,
                                  label: snapshot.data,
                                  onChanged:
                                      menuController.onVolumeSliderChange,
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: ScreenSize.verticalBlockSize * 2,
              ),
              Container(
                height: ScreenSize.verticalBlockSize * 3,
                width: ScreenSize.horizontalBlockSize * 80,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: ScreenSize.verticalBlockSize * 3,
                      width: ScreenSize.horizontalBlockSize * 12,
                      child: buildToggleButton(
                        data: menuController.isDisabled,
                        child: Text(
                          "Disable",
                          style: TextStyle(
                            fontSize: ScreenSize.verticalBlockSize * 1.2,
                            color: menuController.isDisabled
                                ? Colors.white
                                : Colors.black,
                          ),
                        ),
                        onPressed: menuController.onDisableButtonPressed,
                      ),
                    ),
                    Container(
                      height: ScreenSize.verticalBlockSize * 3,
                      width: ScreenSize.horizontalBlockSize * 12,
                      child: buildToggleButton(
                        data: menuController.isMultiPlay,
                        child: Text(
                          "Multi",
                          style: TextStyle(
                            fontSize: ScreenSize.verticalBlockSize * 1.2,
                            color: menuController.isMultiPlay
                                ? Colors.white
                                : Colors.black,
                          ),
                        ),
                        onPressed: menuController.onMultiplePlayButtonPressed,
                      ),
                    ),
                    Container(
                      height: ScreenSize.verticalBlockSize * 3,
                      width: ScreenSize.horizontalBlockSize * 12,
                      child: buildToggleButton(
                        data: menuController.isRepeat,
                        child: Icon(
                          Icons.repeat,
                          color: menuController.isRepeat
                              ? Colors.white
                              : Colors.black,
                        ),
                        onPressed: menuController.onRepeatButtonPressed,
                      ),
                    ),
                    Container(
                      height: ScreenSize.verticalBlockSize * 3,
                      width: ScreenSize.horizontalBlockSize * 12,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                      child: StreamBuilder(
                        stream: menuController.getButtonColorStream,
                        builder: (context, snapshot) {
                          return FlatButton(
                            padding: EdgeInsets.zero,
                            shape: CircleBorder(
                              side: BorderSide(color: Colors.black),
                            ),
                            color: snapshot?.data ??
                                widget.homeController
                                    .getButtonColor(widget.buttonIndex),
                            child: Container(),
                            onPressed: () {
                              menuController.selectColor(true);
                            },
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
            height: ScreenSize.verticalBlockSize * 5,
            width: ScreenSize.horizontalBlockSize * 80,
            padding: EdgeInsets.symmetric(
              horizontal: ScreenSize.horizontalBlockSize,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                MaterialButton(
                  child: Text(
                    "Cancel",
                    style: TextStyle(
                      fontSize: ScreenSize.verticalBlockSize * 1.5,
                    ),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                Container(
                  height: ScreenSize.verticalBlockSize * 3,
                  width: ScreenSize.horizontalBlockSize * 0.2,
                  color: Colors.grey,
                ),
                MaterialButton(
                  child: Text(
                    "Delete",
                    style: TextStyle(
                      fontSize: ScreenSize.verticalBlockSize * 1.5,
                    ),
                  ),
                  onPressed: () {
                    widget.homeController.removePadButton(widget.buttonIndex);
                    Navigator.pop(context);
                  },
                ),
                Container(
                  height: ScreenSize.verticalBlockSize * 3,
                  width: ScreenSize.horizontalBlockSize * 0.2,
                  color: Colors.grey,
                ),
                MaterialButton(
                  child: Text(
                    "Done",
                    style: TextStyle(
                      fontSize: ScreenSize.verticalBlockSize * 1.5,
                    ),
                  ),
                  onPressed: () async {
                    bool isSaveSuccess = await menuController.saveSetting();
                    if (isSaveSuccess) {
                      Navigator.pop(context);
                    } else {
                      //Todo: add failed message
                    }
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildColorSelect() {
    Color _tempColor = (menuController.buttonColorValue != null)
        ? Color(menuController.buttonColorValue)
        : widget.homeController.getButtonColor(widget.buttonIndex);

    return Container(
      height: ScreenSize.verticalBlockSize * 30,
      width: ScreenSize.horizontalBlockSize * 100,
      padding: EdgeInsets.symmetric(
        vertical: ScreenSize.verticalBlockSize * 2,
        horizontal: ScreenSize.horizontalBlockSize * 5,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(20.0)),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: ScreenSize.verticalBlockSize * 21,
            width: ScreenSize.horizontalBlockSize * 90,
            child: MaterialColorPicker(
              circleSize: ScreenSize.verticalBlockSize * 4,
              onMainColorChange: (Color color) {
                _tempColor = color;
              },
              onColorChange: (Color color) {
                _tempColor = color;
              },
              selectedColor: _tempColor,
            ),
          ),
          Container(
            height: ScreenSize.verticalBlockSize * 5,
            width: ScreenSize.horizontalBlockSize * 80,
            padding: EdgeInsets.symmetric(
              horizontal: ScreenSize.horizontalBlockSize,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                MaterialButton(
                  child: Text(
                    "Cancel",
                    style: TextStyle(
                      fontSize: ScreenSize.verticalBlockSize * 1.5,
                    ),
                  ),
                  onPressed: () {
                    menuController.changeMenuPage(0);
                  },
                ),
                Container(
                  height: ScreenSize.verticalBlockSize * 3,
                  width: ScreenSize.horizontalBlockSize * 0.2,
                  color: Colors.grey,
                ),
                MaterialButton(
                  child: Text(
                    "Default",
                    style: TextStyle(
                      fontSize: ScreenSize.verticalBlockSize * 1.5,
                    ),
                  ),
                  onPressed: () {
                    menuController.resetColor();
                  },
                ),
                Container(
                  height: ScreenSize.verticalBlockSize * 3,
                  width: ScreenSize.horizontalBlockSize * 0.2,
                  color: Colors.grey,
                ),
                MaterialButton(
                  child: Text(
                    "Done",
                    style: TextStyle(
                      fontSize: ScreenSize.verticalBlockSize * 1.5,
                    ),
                  ),
                  onPressed: () async {
                    menuController.changeColor(_tempColor);
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildToggleButton(
      {@required bool data,
      @required Widget child,
      @required ValueChanged<bool> onPressed}) {
    return Container(
      child: FlatButton(
        padding: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
          side: BorderSide(
            color: Colors.black,
          ),
        ),
        color: (data) ? Colors.blue : Colors.white,
        child: Center(
          child: child,
        ),
        onPressed: () {
          setState(() {
            data = !data;
            onPressed(data);
          });
        },
      ),
    );
  }
}
