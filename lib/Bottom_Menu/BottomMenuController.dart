import 'dart:async';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:meme_speed/HomeScreen/HomeController.dart';
import 'package:path/path.dart' as pathLib;
import 'package:rxdart/rxdart.dart';

class BottomMenuController {
  HomeController homeController;
  int buttonIndex;

  String _name = "";
  String _path = "No file selected";
  bool _isDisabled = false;
  bool _repeat = false;
  bool _multiPlay = false;
  int _buttonColorValue;
  int _textColorValue;
  double _speed = 1.0;
  double _volume = 1.0;

  AudioPlayer tempPlayer;

  TextEditingController textController;

  CarouselSlider carouselSlider;

  bool isSelectingButtonColorValue = true;

  BottomMenuController({
    @required this.homeController,
    @required this.buttonIndex,
    @required this.tempPlayer,
  }) {
    if (homeController.isButtonRegistered(buttonIndex)) {
      _name = homeController.getButtonName(buttonIndex);
      _path = homeController.getButtonPath(buttonIndex);
      _repeat = homeController.isRepeat(buttonIndex);
      _multiPlay = homeController.isMultiPlay(buttonIndex);
      _buttonColorValue = homeController.getButtonColorValue(buttonIndex);
      _textColorValue = homeController.getTextColorValue(buttonIndex);
      _speed = homeController.getButtonSpeed(buttonIndex);
      _volume = homeController.getButtonVolume(buttonIndex);
    }

    _isDisabled = homeController.isButtonDisable(buttonIndex);

    _speedController.stream.listen(_updateSpeed);
    _volumeController.stream.listen(_updateVolume);
    _buttonColorController.stream.listen(_buildNewButtonColorSelect);
    _textColorController.stream.listen(_buildNewTextColorSelect);

    textController = TextEditingController(text: _name);

    homeController.selectFile.add(pathLib.basename(path));
    changeSpeed.add(speed);
    changeVolume.add(volume);
    setButtonColorValue.add((buttonColorValue != null)
        ? buttonColorValue
        : homeController.getButtonColor(buttonIndex).value);
    setTextColorValue.add((textColorValue != null)
        ? textColorValue
        : homeController.getTextColor(buttonIndex).value);
  }

  String get name {
    return _name;
  }

  set name(String newName) {
    if (newName != _name) {
      _name = newName;
    }
  }

  String get path {
    return _path;
  }

  set path(String newPath) {
    if (newPath.isNotEmpty && newPath != _path) {
      _path = newPath;
    }
  }

  bool isPathEmpty() {
    return path.isEmpty;
  }

  bool get isDisabled {
    return _isDisabled;
  }

  set isDisabled(bool newStatus) {
    if (newStatus != _isDisabled) {
      _isDisabled = newStatus;
    }
  }

  bool get isRepeat {
    return _repeat;
  }

  set isRepeat(bool newStatus) {
    if (newStatus != _repeat) {
      _repeat = newStatus;
    }
  }

  bool get isMultiPlay {
    return _multiPlay;
  }

  set isMultiPlay(bool newStatus) {
    if (newStatus != _multiPlay) {
      _multiPlay = newStatus;
    }
  }

  int get buttonColorValue {
    return _buttonColorValue;
  }

  set buttonColorValue(int newbuttonColorValue) {
    _buttonColorValue = newbuttonColorValue;
  }

  int get textColorValue {
    return _textColorValue;
  }

  set textColorValue(int newtextColorValue) {
    _textColorValue = newtextColorValue;
  }

  double get speed {
    return _speed;
  }

  set speed(double newSpeed) {
    if (newSpeed >= 0.5 && newSpeed <= 2.0) {
      _speed = newSpeed;
    }
  }

  double get volume {
    return _volume;
  }

  set volume(double newVolume) {
    if (newVolume >= 0.0 && newVolume <= 1.0) {
      _volume = newVolume;
    }
  }

  //do not need to check available since the player.play() will check it later
  void playMeme() {
    tempPlayer.play();
  }

  void pauseMeme() {
    tempPlayer.pause();
  }

  Future<void> stopMeme() async {
    await tempPlayer.seek(Duration(milliseconds: 0));
    tempPlayer.stop();
  }

  Future<void> onPlayButtonPressed() async {
    if (tempPlayer.playbackState == AudioPlaybackState.playing) {
      pauseMeme();
    } else if (tempPlayer.playbackState == AudioPlaybackState.completed) {
      await tempPlayer.seek(Duration(milliseconds: 0));
      playMeme();
    } else {
      playMeme();
    }
  }

  void onDisableButtonPressed(bool newValue) {
    isDisabled = newValue;
  }

  void onMultiplePlayButtonPressed(bool newValue) {
    if (path.isNotEmpty && path != "No file selected") {
      isMultiPlay = newValue;
    }
  }

  void onRepeatButtonPressed(bool newValue) {
    if (path.isNotEmpty && path != "No file selected") {
      isRepeat = newValue;
    }
  }

  void changeMenuPage(int pageIndex) {
    if (pageIndex > 0 || pageIndex <= carouselSlider.itemCount - 1) {
      carouselSlider.animateToPage(pageIndex,
          duration: Duration(
            milliseconds: 200,
          ),
          curve: Curves.easeIn);
    }
  }

  AudioPlaybackState getPlayerState() {
    return tempPlayer.playbackState;
  }

  bool isPlayStopButtonAvailable() {
    return tempPlayer.playbackState == AudioPlaybackState.playing ||
        tempPlayer.playbackState == AudioPlaybackState.stopped ||
        tempPlayer.playbackState == AudioPlaybackState.paused;
  }

  void clearTextFiled() {
    textController.clear();
    textController.text = "";
    name = "";
  }

  void resetSetting() {
    clearTextFiled();
    homeController.selectFile.add("No file selected");
    path = "No file selected";
    isDisabled = false;
    isRepeat = false;
    isMultiPlay = false;
    buttonColorValue = null;
    textColorValue = null;
    changeSpeed.add(1.0);
    changeVolume.add(1.0);
  }

  Future<void> browseFile() async {
    String temp = textController.text;
    path = await homeController.browseFile();
    if (temp.isNotEmpty) {
      textController.text = temp;
    } else {
      textController.text = pathLib.basenameWithoutExtension(path);
    }
    tempPlayer.setFilePath(path);
  }

  Future<bool> saveSetting() async {
    name = textController.text;
    if ((name.isNotEmpty && path != "No file selected") ||
        (name.isEmpty && path == "No file selected")) {
      if (!homeController.isButtonRegistered(buttonIndex)) {
        return await homeController.createPadButton(
            buttonIndex,
            name,
            (path != "No file selected") ? path : "",
            isDisabled,
            isRepeat,
            isMultiPlay,
            buttonColorValue,
            textColorValue,
            speed,
            volume);
      } else {
        if (!homeController.isMultiPlay(buttonIndex) &&
            homeController.isMultiPlay(buttonIndex) == isMultiPlay) {
          if (speed != homeController.getButtonSpeed(buttonIndex)) {
            homeController.changeSinglePlayerSpeed(buttonIndex, speed);
          }
          if (volume != homeController.getButtonVolume(buttonIndex)) {
            homeController.changeSinglePlayerVolume(buttonIndex, volume);
          }
        }
        return await homeController.updatePadButton(
            buttonIndex,
            name,
            (path != "No file selected") ? path : "",
            isDisabled,
            isRepeat,
            isMultiPlay,
            buttonColorValue,
            textColorValue,
            speed,
            volume);
      }
    }
    return false;
  }

  void selectColor(bool isSelectingButtonColorValue) {
    if (path.isNotEmpty && path != "No file selected") {
      this.isSelectingButtonColorValue = isSelectingButtonColorValue;
      changeMenuPage(1);
    }
  }

  void changeColor(Color tempColor) {
    if (isSelectingButtonColorValue) {
      buttonColorValue = tempColor.value;
      setButtonColorValue.add(tempColor.value);
      changeMenuPage(0);
    } else {
      textColorValue = tempColor.value;
      setTextColorValue.add(tempColor.value);
      changeMenuPage(0);
    }
  }

  void resetColor() {
    if (isSelectingButtonColorValue) {
      buttonColorValue = null;
      setButtonColorValue.add(null);
      changeMenuPage(0);
    } else {
      textColorValue = null;
      setTextColorValue.add(null);
      changeMenuPage(0);
    }
  }

  void onSpeedSliderChange(double newSpeed) {
    if (path.isNotEmpty && path != "No file selected") {
      changeSpeed.add(newSpeed);
    }
  }

  void onVolumeSliderChange(double newVolume) {
    if (path.isNotEmpty && path != "No file selected") {
      changeVolume.add(newVolume);
    }
  }

  Sink<double> get changeSpeed => _speedController.sink;
  final _speedController = StreamController<double>();

  Stream<String> get getSpeedTitle => _getSpeedBuildSubject.stream;
  final _getSpeedBuildSubject = BehaviorSubject<String>();

  void _updateSpeed(double newSpeed) {
    speed = newSpeed;
    _getSpeedBuildSubject.add("x" + speed.toStringAsFixed(1));
    tempPlayer.setSpeed(newSpeed);
  }

  Sink<double> get changeVolume => _volumeController.sink;
  final _volumeController = StreamController<double>();

  Stream<String> get getVolumeTitleStream => _getVolumeBuildSubject.stream;
  final _getVolumeBuildSubject = BehaviorSubject<String>();

  void _updateVolume(double newVolume) {
    volume = newVolume;
    _getVolumeBuildSubject.add((volume * 10).toInt().toString());
    tempPlayer.setVolume(newVolume);
  }

  Sink<int> get setButtonColorValue => _buttonColorController.sink;
  final _buttonColorController = StreamController<int>();

  Stream<Color> get getButtonColorStream => _getButtonColorSubject.stream;
  final _getButtonColorSubject = BehaviorSubject<Color>();

  void _buildNewButtonColorSelect(int buttonColorValue) {
    if (buttonColorValue != null) {
      _getButtonColorSubject.add(Color(buttonColorValue));
    } else {
      _getButtonColorSubject.add(homeController.getButtonColor(buttonIndex));
    }
  }

  Sink<int> get setTextColorValue => _textColorController.sink;
  final _textColorController = StreamController<int>();

  Stream<Color> get getTextColorStream => _getTextColorSubject.stream;
  final _getTextColorSubject = BehaviorSubject<Color>();

  void _buildNewTextColorSelect(int textColorValue) {
    if (textColorValue != null) {
      _getTextColorSubject.add(Color(textColorValue));
    } else {
      _getTextColorSubject.add(homeController.getTextColor(buttonIndex));
    }
  }

  void dispose() {
    _speedController.close();
    _getSpeedBuildSubject.close();

    _volumeController.close();
    _getVolumeBuildSubject.close();

    _buttonColorController.close();
    _getButtonColorSubject.close();

    _textColorController.close();
    _getTextColorSubject.close();
  }
}
